const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const {mongodbURI, cookieKey} = require('./config/keys');
require('./models/User');
require('./services/passport');


mongoose.connect(mongodbURI);

const app = express();
app.use(cookieSession({
    maxAge: 30*24*60*60*1000,
    keys: [cookieKey]
}));

app.use(passport.initialize());
app.use(passport.session());
require('./Routes/authRoutes')(app);

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
