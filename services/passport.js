const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const {googleClientID, googleClientSecret} = require('../config/keys');
const mongoose = require('mongoose');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
    done(null, user.id); // This is the mongodb ID
});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    });
});

passport.use(new GoogleStrategy({
    clientID: googleClientID,
    clientSecret: googleClientSecret,
    callbackURL: '/auth/google/callback',
    proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
        // console.log(accessToken);
        // console.log(refreshToken);
        // console.log(profile);
        const user = await User.findOne({googleID: profile.id});
        if (user) {
                // user exists with given googleID.
                done(null, user);
        } else {
            const user = await new User({googleID: profile.id}).save();
            done(null, user);
        });

    })
); // Creates a new instance of a GoogleStrategy
